# this is a few simple integration tests for an upPIR mirror.   Uses any local
# mirror

# if everything passes, there is no output

import socket


import getmyip

import httplib

import urllib

mirrortocheck = getmyip.getmyip()

def get_response(requeststring):
  mirrorcx = httplib.HTTPConnection(mirrortocheck, 62294)
  params = urllib.urlencode({'request': requeststring, 'bstring': '""'}) + '\n'
  headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
  mirrorcx.request("POST", '', params, headers)
  mresponse = mirrorcx.getresponse()
  return mresponse.reason
  

# We don't know the size so we won't test it 'working'...

# are you friendly?
assert('HI!' == get_response('HELLO'))

# too short of a string (len 0)
assert('Invalid request length' == get_response('XORBLOCK'))

# too short of a string (len 0)
assert('Invalid request type' == get_response('ajskdfjsad'))
