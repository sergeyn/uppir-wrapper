#!/bin/bash

CLIENT_DIR="client"

MIRROR1_DIR="mirror1"

MIRROR2_DIR="mirror2"

MIRROR3_DIR="mirror3"

ANONIMIZER1_DIR="anonimizer1"

ANONIMIZER2_DIR="anonimizer2"

ANONIMIZER3_DIR="anonimizer3"

VENDOR_DIR="vendor"

echo "Switching to directory '$VENDOR_DIR'"
cd $VENDOR_DIR

echo "run the vendor"
python uppir_new_vendor.py --foreground &
sleep 3
echo "Switching to directory '$MIRROR1_DIR'"
cd - 
cd $MIRROR1_DIR

echo "run the mirror1"
python uppir_new_mirror.py --ip=127.0.0.1 --port 62001 --foreground --mirrorroot=../filestoshare/ --httpport=61001 --retrievemanifestfrom=127.0.0.1 &
sleep 3
echo "Switching to directory '$MIRROR2_DIR'"
cd - 
cd $MIRROR2_DIR

echo "run the mirror2"
python uppir_new_mirror.py --ip=127.0.0.1 --port 62002 --foreground --mirrorroot=../filestoshare/ --httpport 61002 --retrievemanifestfrom=127.0.0.1 &
sleep 3

echo "Switching to directory '$MIRROR3_DIR'"
cd - 
cd $MIRROR3_DIR

echo "run the mirror3"
python uppir_new_mirror.py --ip=127.0.0.1 --port 62003 --foreground --mirrorroot=../filestoshare/ --httpport=61003 --retrievemanifestfrom=127.0.0.1 &
sleep 3

echo "Switching to directory '$ANONIMIZER1_DIR'"
cd - 
cd $ANONIMIZER1_DIR

echo "run the anonimizer1"
python uppir_new_anonimizer.py  --ip=127.0.0.1 --port 62010 --foreground --mirrorroot=../filestoshare/ --httpport=61010 --retrievemanifestfrom=127.0.0.1&
sleep 3

echo "Switching to directory '$ANONIMIZER2_DIR'"
cd - 
cd $ANONIMIZER2_DIR

echo "run the anonimizer2"
python uppir_new_anonimizer.py --ip=127.0.0.1 --port 62011 --foreground --mirrorroot=../filestoshare/ --httpport=61011 --retrievemanifestfrom=127.0.0.1&
sleep 3

echo "Switching to directory '$ANONIMIZER3_DIR'"
cd - 
cd $ANONIMIZER3_DIR

echo "run the anonimizer3"
python uppir_new_anonimizer.py --ip=127.0.0.1 --port 62012 --foreground --mirrorroot=../filestoshare/ --httpport=61012 --retrievemanifestfrom=127.0.0.1&
sleep 3

echo "Switching to directory '$CLIENT_DIR'"
cd - 
cd $CLIENT_DIR


echo "python uppir_new_client.py LICENSE.TXT"
